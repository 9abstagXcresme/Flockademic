jest.mock('react-ga');
jest.mock('../../src/services/periodical', () => ({
  initialiseArticle: jest.fn().mockReturnValue(Promise.resolve({ identifier: 'arbitrary article id' })),
}));
jest.mock('../../src/services/orcid', () => ({
  getWork: jest.fn().mockReturnValue(Promise.resolve({ name: 'Arbitrary ORCID Work name' })),
}));

import { ArticleInitialisationPage } from '../../src/components/articleInitialisationPage/component';
import { Spinner } from '../../src/components/spinner/component';

import { mount, shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

const routerProps = {
  params: {
    periodicalId: 'Arbitrary periodical slug',
  },
};

it('should display a spinner while initialising the journal', () => {
  const page = shallow(<ArticleInitialisationPage match={routerProps}/>);

  expect(page.find(Spinner)).toExist();
});

it('should display an error message when something went wrong initialising the article', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.initialiseArticle.mockReturnValueOnce(Promise.reject('Initialisation error'));

  const page = shallow(<ArticleInitialisationPage match={routerProps}/>);

  setImmediate(() => {
    page.update();
    expect(page.find('.callout.alert')).toExist();
    done();
  });
});

it('should base the new article on an ORCID work if a reference to one is provided', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  const mockedOrcidService = require.requireMock('../../src/services/orcid');
  mockedOrcidService.getWork.mockReturnValueOnce(Promise.resolve({
    name: 'Some ORCID Work name',
  }));

  const mockRouterProps = {
    ...routerProps,
    params: {
      orcidWorkId: '0000-0002-4013-9889:1337',
    },
  };

  const page = shallow(<ArticleInitialisationPage match={mockRouterProps}/>);

  setImmediate(() => {
    expect(mockedOrcidService.getWork.mock.calls.length).toBe(1);
    expect(mockedOrcidService.getWork.mock.calls[0][0]).toBe('0000-0002-4013-9889');
    expect(mockedOrcidService.getWork.mock.calls[0][1]).toBe('1337');
    expect(mockedPeriodicalService.initialiseArticle.mock.calls.length).toBe(1);
    expect(mockedPeriodicalService.initialiseArticle.mock.calls[0][1])
      .toEqual({ name: 'Some ORCID Work name', sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337' });
    done();
  });
});

it('should also base the new article on an ORCID work if it is by an ORCID ID that ends in an X', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  const mockedOrcidService = require.requireMock('../../src/services/orcid');
  mockedOrcidService.getWork.mockReturnValueOnce(Promise.resolve({
    name: 'Some ORCID Work name',
  }));

  const mockRouterProps = {
    ...routerProps,
    params: {
      orcidWorkId: '0000-0002-4013-988X:1337',
    },
  };

  const page = shallow(<ArticleInitialisationPage match={mockRouterProps}/>);

  setImmediate(() => {
    expect(mockedOrcidService.getWork.mock.calls[0][0]).toBe('0000-0002-4013-988X');
    expect(mockedPeriodicalService.initialiseArticle.mock.calls[0][1])
      .toEqual({ name: 'Some ORCID Work name', sameAs: 'https://orcid.org/0000-0002-4013-988X/work/1337' });
    done();
  });
});

it('should send an analytics event when initialising a new article based off an ORCID work', (done) => {
  const mockedReactGa = require.requireMock('react-ga');

  const mockRouterProps = {
    ...routerProps,
    params: {
      orcidWorkId: '0000-0002-4013-9889:1337',
    },
  };

  const page = shallow(<ArticleInitialisationPage match={mockRouterProps}/>);

  setImmediate(() => {
    expect(mockedReactGa.event.mock.calls.length).toBe(1);
    expect(mockedReactGa.event.mock.calls[0][0]).toEqual({
      action: 'Initialise new article based on an ORCID work',
      category: 'Article Management',
    });

    done();
  });
});

it('should redirect to the management page after initialising a new Article', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.initialiseArticle.mockReturnValueOnce(Promise.resolve({
    result: { identifier: 'some_uuid' },
  }));

  const page = shallow(<ArticleInitialisationPage match={routerProps}/>);

  setImmediate(() => {
    page.update();
    expect(page.find('Redirect[to="/article/some_uuid/manage"]')).toExist();
    done();
  });
});
